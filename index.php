<?php
require_once("db_connect.php"); ?>

<html>
<head>
    <title>My Todo's</title>
</head>
<body>
<h2>
    Next on my agenda
</h2>
<a href="create.php">add todo</a>
<ul>
<?php
db();
global $link;
$query = "SELECT id, todoTitle, todo, date FROM todos";
$result = mysqli_query($link, $query);
if(mysqli_num_rows($result) >= 1) {
    while($row = mysqli_fetch_array($result)){
$id = $row['id'];
$title = $row['todoTitle'];
$date = $row['date'];
?>

    <li>
        <a href="detail.php?id=<?php echo $id?>"><?php echo $title ?>
        </a>
    </li><?php echo "[[$date]]"; ?>
    <?php
    }
}
?>

</ul>
</body>
</html>