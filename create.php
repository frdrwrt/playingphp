

<?php
require_once("db_connect.php");
$titleError = $descriptionError = "";
$title = $description = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["todoTitle"])) {
        $titleError = "Title is required";
    } else {
        $title = test_input($_POST["todoTitle"]);
    }
    if (empty($_POST["todoDescription"])) {
        $descriptionError = "Description is required";
    } else {
        $description = test_input($_POST["todoDescription"]);
    }
    insertDB($title, $description);
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function insertDB($title, $description)
{
    db();
    global $link;
    $query = "INSERT INTO todos(todoTitle, todo, date) 
VALUES ('$title', '$description', now())";
    $insertTodo = mysqli_query($link, $query);
    if ($insertTodo) {
        echo "successfully";
    } else {
        echo mysqli_error($link);
    }
    mysqli_close($link);
}

?>
<html>
<head>
    <title>TodoList</title>
</head>
<body>
<h1>Todo List</h1>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="todoTitle">Todo title:</label>
    <input name="todoTitle" id="todoTitle" type="text"><br>
    <span class="error">* <?php echo $titleError;?></span><br>
    <label for="todoDescription">Todo description:</label>
    <input name="todoDescription" id="todoDescription" type="text"><br>
    <span class="error">* <?php echo $descriptionError;?></span><br>
    <br>
    <input type="submit" name="submit" value="submit">
</form>
</body>
</html>
